/** @jsx React.DOM */

var React = require('react');

var ShowAddButton = React.createClass({

	render: function() {

		var classString, buttonText;
		
		if (this.props.isdisplayed) {
			classString = 'btn btn-default btn-block';
			buttonText = 'Cancel';
		} else {
			classString = 'btn btn-success btn-block';
			buttonText = 'Create New Item';
		}

		return (
      <button 
      	onClick={this.props.onToggleForm} 
      	className={classString} >
      	{buttonText}
      </button>
		);
	}

});

module.exports = ShowAddButton;